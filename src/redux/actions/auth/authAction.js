import Cookie from "js-cookie";
import Cookies from "js-cookie";
import { API } from "../../../config";

export const removedata = (cb) => async () => {
  await Cookie.remove('user')
  return cb()
}

export const setLogout = (successCB) => async () => {
  Cookies.remove('user')
  window.location.reload()
  return successCB && successCB();
}

export const checkAuth = () => async dispatch => {
  await dispatch({ type: 'LOAD_DATA_AUTH' })
  const getToken = Cookie.get('user')
  if(getToken){
    if(getToken !== null){
      return dispatch({ 
        type: 'LOAD_DATA_AUTH_SUCCESS',
      })
    }else {
      return dispatch(setLogout());
    }
  }else{
    return dispatch({ type: 'LOAD_DATA_AUTH_FAILED' })
  }
}

export const setLogin = (value, successCB, failedCB) => async dispatch => {
  await dispatch({ type: 'LOAD_DATA_AUTH' })
  API.POST('/auth/local', value).then((response) => {
    const token = response.data.jwt
    Cookie.set('user', token)
    dispatch({ type: 'LOAD_DATA_AUTH_SUCCESS'
  })
    return successCB && successCB(response)
  }).catch((err) => {
    dispatch({ type: 'LOAD_DATA_AUTH_FAILED' })
    return failedCB && failedCB(err)
  })
}

export const setRegister = (value, successCB, failedCB) => async dispatch => {
  await dispatch({ type: 'LOAD_DATA_AUTH' })
  API.POST('/auth/local/register', value).then((response) => {
    const token = response.data.jwt
    Cookie.set('user', token)
    dispatch({ type: 'LOAD_DATA_AUTH_SUCCESS'
  })
    return successCB && successCB(response)
  }).catch((err) => {
    dispatch({ type: 'LOAD_DATA_AUTH_FAILED' })
    return failedCB && failedCB(err)
  })
}