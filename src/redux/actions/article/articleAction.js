import { API } from '../../../config'

export const unmountListArticle = () => (dispatch) => {
  return dispatch({ type: 'UNMOUNT_LIST_DATA_ARTICLE' })
}

export const unmountDetailArticle = () => (dispatch) => {
  return dispatch({ type: 'UNMOUNT_DETAIL_DATA_ARTICLE' })
}

export const listArticle = (page, pageSize, value, field, operator) => async (dispatch) => {
  const params = {
    "pagination[page]": page,
    "pagination[pageSize]": pageSize,
    [value ? `filters[${value && field}][${value && operator}]` : '']: value ? value : ''
  }
  await dispatch({ type: 'LIST_DATA_ARTICLE' })
  return API.GET(`/articles`, params).then((response) => {
    dispatch({
      type: 'LIST_DATA_ARTICLE_SUCCESS', 
      payload: {
        data: response.data,
        pagination: {
          page: response.meta.pagination.page,
          total: response.meta.pagination.total,
          pageSize: response.meta.pagination.pageSize
        }
    }})
  }).catch((err) => {
    return dispatch({ type: 'LIST_DATA_ARTICLE_FAILED' })
  })
}

export const detailArticle = (id) => async dispatch => {
  await dispatch({ type: 'DETAIL_DATA_ARTICLE' })
  return API.GET(`/articles/${id}`)
  .then((response) => {
    const data = response.data
    dispatch({
      type: 'DETAIL_DATA_ARTICLE_SUCCESS', 
      payload: {
        data: data,
      }
    })
  }).catch(() => {
    dispatch({ type: 'DETAIL_DATA_ARTICLE_FAILED'})
  })
}

export const createArticle = (value, successCB, failedCB) => () => {
  API.POST('/articles', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return failedCB && failedCB(err)
  })
}

export const editArticle = (id, value, successCB, failedCB) => () => {
  API.POST_EDIT(`/articles/${id}`, value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return failedCB && failedCB(err)
  })
}

export const deleteArticle = (id, successCB, failedCB) => () => {
  API.DELETE(`/articles/${id}`).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return failedCB && failedCB(err)
  })
}