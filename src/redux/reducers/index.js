import { combineReducers } from 'redux';
import authReducer from './auth/authReducer';
import listArticleReducer from './article/listArticleReducer';
import detailArticleReducer from './article/detailArticleReducer';

export default combineReducers({
  authReducer,
  article: combineReducers({
    list: listArticleReducer,
    detail: detailArticleReducer
  })
})