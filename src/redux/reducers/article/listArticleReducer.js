const LIST_DATA_ARTICLE           = 'LIST_DATA_ARTICLE';
const LIST_DATA_ARTICLE_SUCCESS   = 'LIST_DATA_ARTICLE_SUCCESS';
const LIST_DATA_ARTICLE_FAILED    = 'LIST_DATA_ARTICLE_FAILED';
const UNMOUNT_LIST_DATA_ARTICLE   = 'UNMOUNT_LIST_DATA_ARTICLE';

const initialState = {
  data: [],
  loading: true,
  message: null,
  pagination: {
    page: 1,
    total: 0,
    pageSize: 10
  }

}

const listArticleReducer = (state = initialState, action) => {
  switch(action.type){
    case LIST_DATA_ARTICLE:
      return {
        ...state,
        loading: true,
        data: null,
        pagination: {
          page: 1,
          total: 0,
          pageSize: 10
        }

      }
    case LIST_DATA_ARTICLE_SUCCESS:
      return { 
        ...state, 
        loading: false,
        data: action.payload.data,
        message: 'SUCCESS',
        pagination: {
          total: action.payload.pagination.total,
          page: action.payload.pagination.page,
          pageSize: action.payload.pagination.pageSize
        }
      }
    case LIST_DATA_ARTICLE_FAILED:
      return { 
        ...state, 
        loading: true,
        data: null,
        message: 'FAILED',
        pagination: {
          page: 1,
          total: 0,
          pageSize: 10
        }
      }
    case UNMOUNT_LIST_DATA_ARTICLE:
      return { 
        ...state, 
        loading: true,
        data: null,
        pagination: {
          page: 1,
          total: 0,
          pageSize: 10
        }
      }
    default:
      return state
  }
}

export default listArticleReducer;