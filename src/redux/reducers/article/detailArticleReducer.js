const DETAIL_DATA_ARTICLE = 'DETAIL_DATA_ARTICLE';
const DETAIL_DATA_ARTICLE_SUCCESS = 'DETAIL_DATA_ARTICLE_SUCCESS';
const DETAIL_DATA_ARTICLE_FAILED  = 'DETAIL_DATA_ARTICLE_FAILED';
const UNMOUNT_DETAIL_DATA_ARTICLE = 'UNMOUNT_DETAIL_DATA_ARTICLE';

const initialState = {
  data: null,
  loading: true,
  message: null
}

const detailArticleReducer = (state = initialState, action) => {
  switch(action.type){
    case DETAIL_DATA_ARTICLE:
      return {
        ...state,
        data: null,
        loading: true,
      }
    case DETAIL_DATA_ARTICLE_SUCCESS:
      return { 
        ...state, 
        loading: false,
        message: 'SUCCESS',
        data: action.payload.data,
      }
    case DETAIL_DATA_ARTICLE_FAILED:
      return { 
        ...state, 
        data: null,
        loading: true,
        message: 'FAILED',
      }
    case UNMOUNT_DETAIL_DATA_ARTICLE:
      return { 
        ...state, 
        data: null,
        loading: true,
      }
    default:
      return state
  }
}

export default detailArticleReducer;