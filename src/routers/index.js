import React from 'react';
import { Navigate } from 'react-router-dom';

const Login = React.lazy(() => import('../pages/Login'));
const Registration = React.lazy(() => import('../pages/Registration'));
const Articles = React.lazy(() => import('../pages/Articles'));
const CreateArticle = React.lazy(() => import('../pages/Articles/Create'));
const EditArticle = React.lazy(() => import('../pages/Articles/Edit'));

export const publicRouter = [
  {
    index: false,
    path: '/login',
    name: 'Login',
    key: 'login',
    element: <Login />,
  },
  {
    index: false,
    path: '/registration',
    name: 'Registration',
    key: 'registration',
    element: <Registration />,
  },
  {
    index: false,
    path: '*',
    name: 'Not Found',
    key: '*',
    element: <Navigate to='/login' />,
  }
]

export const privateRouter = [
  {
    index: false,
    path: '/',
    name: 'Articles',
    key: '/',
    element: <Articles />,
  },
  {
    index: false,
    path: '/article/create',
    name: 'CreateArticle',
    key: 'createArticle',
    element: <CreateArticle />,
  },
  {
    index: false,
    path: '/article/edit/:id',
    name: 'EditArticle',
    key: 'editArticle',
    element: <EditArticle />,
  },
  {
    index: false,
    path: '*',
    name: 'Not Found',
    key: '*',
    element: <Navigate to='/' />,
  }
]