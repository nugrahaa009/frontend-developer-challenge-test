import React, { useEffect, Suspense } from 'react';
import { LayoutPrivate, Loading } from './components';
import { useSelector, useDispatch } from 'react-redux'
import { privateRouter, publicRouter } from './routers';
import { checkAuth } from './redux/actions/auth/authAction';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';

const App = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(checkAuth())
  }, [dispatch])

  const getAuth = useSelector((state) => state.authReducer)

  const PublicRoute = ({ authed, children }) => {
    if (authed) {
      return <Navigate to="/" replace />;
    }
    return children;
  };

  const PrivateRoute = ({ authed, children }) => {
    if (!authed) {
      return <Navigate to="/login" replace />;
    }
    return children;
  };

  if (getAuth.loading) return <Loading />
  
  return (
    <BrowserRouter>
      <Routes>
        {
          !getAuth.authed ? 
            <>
              {
                publicRouter.map((item) => (
                  <Route
                    key={item.key}
                    path={item.path}
                    element={
                      <Suspense fallback={<Loading />}>
                        <PublicRoute authed={getAuth.authed}>
                          {item.element}
                        </PublicRoute>
                      </Suspense>
                    }
                  />
                ))
              }
            </>
          :
            <>
              {
                privateRouter.map((item) => (
                  <Route
                    key={item.key}
                    path={item.path}
                    element={
                      <Suspense fallback={<Loading />}>
                        <PrivateRoute authed={getAuth.authed}>
                          <LayoutPrivate>
                            {item.element}
                          </LayoutPrivate>
                        </PrivateRoute>
                      </Suspense>
                    }
                  />
                ))
              }
            </>
        }
      </Routes>
    </BrowserRouter>
  )
}

export default App;