import React from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setLogin } from "../../redux/actions/auth/authAction";
import { Form, Row, Col, Card, Typography, Input, Button, message } from "antd";

const Login = () => {
  const dispatch = useDispatch();

  const onFinish = (values) => {
    return dispatch(setLogin(values, null, (err) => message.error(err)))
  }

  return (
    <Form onFinish={onFinish} layout='vertical'>
      <Row gutter={16} style={{ height: '100vh', display: 'flex', alignContent: 'center', background: '#fafafa' }}>
        <Col span={24} style={{ display: 'flex', justifyContent: 'center' }}>
          <Card style={{ width: 600, padding: 30 }}>
            <Row gutter={16}>
              <Col span={24} style={{ display: 'flex', justifyContent: 'center', marginBottom: 30 }}>
                <Typography.Title level={3}>
                  Login
                </Typography.Title>
              </Col>
              <Col span={24}>
                <Form.Item 
                  name='identifier' 
                  rules={[
                    { required: true, message: 'Please input your username!' }
                  ]}
                >
                  <Input size='large' placeholder='Username / Email'  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item 
                  validateFirst
                  name='password' 
                  rules={[
                    { required: true, message: 'Please input your password!' },
                    { min: 8, message: 'The Password must be at least is 8 characters and must contain at least 1 lowercase letter, 1 capital letter, 1 number and 1 special character' },
                    { pattern: /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/, message: 'The Password must be at least is 8 characters and must contain at least 1 lowercase letter, 1 capital letter, 1 number and 1 special character' },
                  ]}
                >
                  <Input.Password size='large' placeholder='Passowrd' />
                </Form.Item>
              </Col>
              <Col span={24} style={{ marginBottom: 20 }}>
                <Button block size='large' type='primary' htmlType='submit'>
                  Login
                </Button>
              </Col>
              <Col span={24} style={{ display: 'flex', justifyContent: 'center' }}>
                <Typography.Text type='secondary'> 
                  Don't have an account yet? <Link to='/registration'>Register now!</Link>
                </Typography.Text>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </Form>
  )
}

export default Login;