import React, { useEffect, useState } from 'react';
import columns from './columns';
import { Link } from 'react-router-dom';
import { PlusOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux'; 
import { Row, Col, PageHeader, Table, Pagination, Input, Button, message } from 'antd';
import { deleteArticle, listArticle, unmountListArticle } from '../../redux/actions/article/articleAction';

const Articles = () => {
  const dispatch = useDispatch();

  const [page, setPage] = useState(1);

  const [pageSize, setPageSize] = useState(10);

  const [value, setValue] = useState(null)

  const [field, setField] = useState(null)

  const [operator, setOperator] = useState(null)

  const onChangePagination = (page, pageSize) => {
    setPage(page)
    setPageSize(pageSize)
  }

  const onSearch = (e) => {
    setValue(e.target.value)
    setField('title')
    setOperator('$containsi')
  }

  const removeArticle = (id) => {
    dispatch(deleteArticle(id), () => {
      message.success('Delete article success')
    })
  }

  useEffect(() => {
    dispatch(listArticle(page, pageSize, value, field, operator))

    return () => {
      dispatch(unmountListArticle())
    }
  }, [dispatch, field, operator, page, pageSize, value])

  const getData = useSelector((item) => item.article.list);

  const { loading, data, pagination } = getData;

  return (
    <Row gutter={[16, 24]}>
      <Col span={24}>
        <PageHeader title='LIST ARTICLE' style={{ padding: 0 }} />
      </Col>
      <Col span={24}>
        <Row gutter={16}>
          <Col span={6}>
            <Input 
              allowClear 
              size='large' 
              onChange={onSearch} 
              placeholder='Search'
              style={{ borderRadius: 6 }} 
            />
          </Col>
          <Col span={18} style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Link to='/article/create'>
              <Button size='large' type='primary' icon={<PlusOutlined />}>
                Create Article
              </Button>
            </Link>
          </Col>
        </Row>
      </Col>
      <Col span={24}>
        <Table
          size='small'
          dataSource={data}
          loading={loading}
          pagination={false}
          rowKey={(i) => i.id}
          columns={columns(page, pageSize, removeArticle)}
        />
      </Col>
      <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Pagination
          total={pagination.total}
          current={pagination.page}
          onChange={onChangePagination}
          pageSize={pagination.pageSize}
        />
      </Col>
    </Row>
  )
}

export default Articles