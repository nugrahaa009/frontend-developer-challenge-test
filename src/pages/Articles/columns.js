import React from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { number } from '../../utils/numberTable';
import { Typography, Space, Button, Popconfirm } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';

const columns = (page, pageSize, removeArticle) => [
  {
    title: 'No',
    key: 'no',
    render: (data, text, index) => number(index, page, pageSize)
  },
  {
    title: 'Title',
    key: 'title',
    render: (record) => (
      <Typography.Text>
        {record.attributes.title}
      </Typography.Text>
    )
  },
  {
    title: 'Content',
    key: 'content',
    render: (record) => (
      <Typography.Text>
        {record.attributes.content}
      </Typography.Text>
    )
  },
  {
    title: 'Created At',
    key: 'createdAt',
    align: 'center',
    sorter: (a, b) => a.createdAt - b.createdAt,
    render: (record) => (
      <Typography.Text>
        {moment(record.attributes.createdAt).format('ll')}
      </Typography.Text>
    )
  },
  {
    title: '',
    key: 'action',
    align: 'right',
    render: (record) => (
      <Space>
        <Link to={`/article/edit/${record.id}`}>
          <Button size='large' type='primary' icon={<EditOutlined />}>
            Edit
          </Button>
        </Link>
        <Popconfirm 
            okText='OK'
            okType='danger'
            cancelText='Cancel'
            placement='topLeft' 
            title='Are you sure delete this cake?' 
            onConfirm={() => removeArticle(record.id)}
          >
            <Button size='large' type='danger' icon={<DeleteOutlined />}>
              Delete
            </Button>
          </Popconfirm>
      </Space>
    )
  }
]

export default columns;