import React, { useEffect } from 'react';
import { LeftOutlined } from '@ant-design/icons';
import { useNavigate, useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Row, Col, PageHeader, Skeleton, Input, Divider, Button, message } from 'antd';
import { detailArticle, editArticle, unmountDetailArticle } from '../../../redux/actions/article/articleAction';

const EditArticle = () => {
  const dispatch = useDispatch();

  const history = useNavigate();

  const params = useParams();

  const onFinish = (values) => {
    const { id } = params;

    const payload = {
      data: {
        title: values.title,
        content: values.content
      }
    }
    dispatch(editArticle(id, payload, () => {
      history('/')
      message.success('Edit article success')
    }, (err) => {
      message.error(err.message)
    }))
  }

  useEffect(() => {
    const { id } = params;
    dispatch(detailArticle(id))
  
    return () => {
      dispatch(unmountDetailArticle())
    }
  }, [dispatch, params])

  const getData = useSelector((state) => state.article.detail);

  const { loading, data } = getData;

  return (
    <Form onFinish={onFinish} layout='vertical'>
      <Row gutter={16}>
        <Col span={24} style={{ marginBottom: 30 }}>
          <PageHeader 
            title='Edit Article'
            style={{ padding: 0 }}
            onBack={() => history('/')}
            backIcon={<LeftOutlined />}
          />
        </Col>
        {
          loading ? (
            <Col span={24}>
              <Skeleton />
            </Col>
          ) : (
            <>
              <Col xs={24} sm={24} md={12} lg={12}>
                <Form.Item
                  name='title'
                  label='Article Name'
                  initialValue={data.attributes.title}
                  rules={[
                    { required: true, message: 'Please input your article name' }
                  ]}
                >
                  <Input size='large' placeholder='Article name' style={{ borderRadius: 6 }} />
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12}>
                <Form.Item
                  name='content'
                  label='Content'
                  initialValue={data.attributes.content}
                  rules={[
                    { required: true, message: 'Please input your content' }
                  ]}
                >
                  <Input size='large' placeholder='Content' style={{ borderRadius: 6 }} />
                </Form.Item>
              </Col>
              <Divider />
              <Col xs={24} sm={24} md={12} lg={12}>
                <Button block type='default' size='large'>
                  CANCEL
                </Button>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12}>
                <Button block type='primary' size='large' htmlType='submit'>
                  SAVE
                </Button>
              </Col>
            </>
          )
        }
      </Row> 
    </Form>
  )
}

export default EditArticle;