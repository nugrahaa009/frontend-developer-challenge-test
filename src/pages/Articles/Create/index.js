import React from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import { LeftOutlined } from '@ant-design/icons';
import { createArticle } from '../../../redux/actions/article/articleAction';
import { Form, Row, Col, PageHeader, Input, Button, Divider, message } from 'antd';

const CreateArticle = () => {
  const history = useNavigate();

  const dispatch = useDispatch();

  const onFinish = (values) => {
    const payload = {
      data: {
        title: values.title,
        content: values.content
      }
    }
    dispatch(createArticle(payload, () => {
      history('/')
      message.success('Create article success')
    }, (err) => {
      message.error(err.message)
    }))
  }

  return (
    <Form onFinish={onFinish} layout='vertical'>
      <Row gutter={16}>
        <Col span={24} style={{ marginBottom: 30 }}>
          <PageHeader 
            title='Create Article'
            style={{ padding: 0 }}
            onBack={() => history('/')}
            backIcon={<LeftOutlined />}
          />
        </Col>
        <Col xs={24} sm={24} md={12} lg={12}>
          <Form.Item
            name='title'
            label='Article Name'
            rules={[
              { required: true, message: 'Please input your article name' }
            ]}
          >
            <Input size='large' placeholder='Article name' style={{ borderRadius: 6 }} />
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={12} lg={12}>
          <Form.Item
            name='content'
            label='Content'
            rules={[
              { required: true, message: 'Please input your content' }
            ]}
          >
            <Input size='large' placeholder='Content' style={{ borderRadius: 6 }} />
          </Form.Item>
        </Col>
        <Divider />
        <Col xs={24} sm={24} md={12} lg={12}>
          <Button block type='default' size='large'>
            CANCEL
          </Button>
        </Col>
        <Col xs={24} sm={24} md={12} lg={12}>
          <Button block type='primary' size='large' htmlType='submit'>
            SAVE
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

export default CreateArticle;