import { removedata } from '../redux/actions/auth/authAction';

const errorHandler = (status, cb) => async dispatch => {
  if (status) {
    switch (status.shortCode) {
      case "Error":
        return null;
      case "UNAUTHORIZED":
        return null; 
      case "FAILED":
        return null; 
      default:
        return cb;
    }
  } else {
    await dispatch({type: 'SET_LOGOUT'});
    return dispatch(removedata(() => {
      return dispatch({type: 'SET_LOGOUT_SUCCESS'});
    }))
  }
}

export default errorHandler;