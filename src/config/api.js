import axios from 'axios';
import { env } from './index';
import Cookie from 'js-cookie';

export const GET = async (path, params) => {
  const getToken = Cookie.get('user');
  const header = {
    "Authorization": `Bearer ${getToken}`,
  }
  return new Promise((resolve ,reject) => {
    axios.get(`${env.API}${path}`, {
      headers: header,
      params: params
    }).then((response) => {
      if(response.status === 200){
        return resolve(response.data);
      }else{
        const err = response.status;
        return reject(err)
      }
    }).catch((err) => {
      const error = err.message
      return reject(error)
    })
  })
}

export const POST_EDIT = (path, payload) => {
  const getToken = Cookie.get('user');
  const header = {
    "Authorization": `Bearer ${getToken}`,
  }
  return new Promise((resolve ,reject) => {
    axios.get(`${env.API}${path}`, {
      headers: header,
      params: payload
    }).then((response) => {
      if(response.status === 200){
        return resolve(response.data);
      }else{
        const err = response.status;
        return reject(err)
      }
    }).catch((err) => {
      const error = err.message
      return reject(error)
    })
  })
}
  
export const POST = (path, payload) => {
  const getToken = Cookie.get('user');
  const header = {
    "Authorization": `Bearer ${getToken ? getToken: ''}`,
  }
  return new Promise((resolve ,reject) => {
    axios.post(`${env.API}${path}`,payload , {
      headers: header
    }).then((response) => {
      if(response.status === 200){
        return resolve(response);
      }else{
        const err = response.status;
        return reject(err)
      }
    }).catch((err) => {
      return reject(err.response.data.error.message)
    })
  })
}

export const DELETE = (path, payload) => {
  const header = {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  }
  return new Promise((resolve ,reject) => {
    axios.delete(`${env.API}${path}`,payload , {
      headers: header
    }).then((response) => {
      if(response.status === 200){
        return resolve(response.data);
      }
    }).catch((err) => {
      const error = err.response ? err.response.status : JSON.parse(JSON.stringify(err));
      return reject(error)
    })
  })
}
 