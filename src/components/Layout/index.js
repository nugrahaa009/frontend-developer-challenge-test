import React from "react";
import './index.css';
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { Layout, Menu, Row, Col } from "antd";
import { setLogout } from "../../redux/actions/auth/authAction";
import { MailOutlined, SettingOutlined, LogoutOutlined } from '@ant-design/icons';
const { Header, Content, Footer } = Layout;

const items = (onLogout) => [
  {
    key: '1',
    label: 'Menu One',
    icon: <MailOutlined />,
  },
  {
    key: '2',
    label: 'Menu Two',
    icon: <SettingOutlined />,
  },
  {
    key: '3',
    icon: <LogoutOutlined />,
    label: (
      <Link to='#' onClick={onLogout}>
        Logout
      </Link>
    ),
  },
];

const LayoutPrivate = (props) => {
  const { children } = props;

  const dispatch = useDispatch();

  const onLogout = () => {
    return dispatch(setLogout())
  }

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Header>
        <Row gutter={16}>
          <Col span={12}>
            <div style={{ height: 32, margin: 16, width: 200, background: 'rgba(255, 255, 255, 0.3)' }} />
          </Col>
          <Col span={12}>
            <Menu
              style={{ justifyContent: 'right' }}
              theme="dark"
              items={items(onLogout)}
              mode="horizontal"
              defaultSelectedKeys={['1']}
            />
          </Col>
        </Row>
      </Header>
      <Content style={{ padding: 50 }}>
        <div className="site-layout-content">
          {children}
        </div>
      </Content>
      <Footer
        style={{
          textAlign: 'center',
        }}
      >
        Ant Design ©2018 Created by Ant UED
      </Footer>
    </Layout>
  )
}

export default LayoutPrivate;