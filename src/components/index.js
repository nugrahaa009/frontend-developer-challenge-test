import Loading from "./Loading";
import LayoutPrivate from "./Layout";

export {
  Loading,
  LayoutPrivate
}